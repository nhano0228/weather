//
//  dailyWeather.swift
//  Weather
//
//  Created by Noah Hanover on 9/22/14.
//  Copyright (c) 2014 Hanover. All rights reserved.
//

import Foundation
import UIKit

struct dailyWeather {
    var icon: UIImage?
    var minTemperature: Int
    var maxTemperature: Int
    var date: String?
	var rain: String?
    
    init(weatherArray: NSArray, dayOftheWeek: Int) {
        let dayDictionary = weatherArray[dayOftheWeek] as NSDictionary
        
        maxTemperature = dayDictionary["temperatureMax"] as Int
        minTemperature = dayDictionary["temperatureMin"] as Int
        
        let currentTime = dayDictionary["time"] as Int
        date = constantFunc().dateStringFromUnixTime(currentTime)
		
		let rainToday = dayDictionary["precipProbability"] as Double
		rain = "\(Int(rainToday*100))%" as String
		
        let iconString = dayDictionary["icon"] as String
        icon = constantFunc().weatherIconFromString(iconString)
        
    }
}