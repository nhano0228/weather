//
//  Hourly.swift
//  Weather
//
//  Created by Noah Hanover on 9/23/14.
//  Copyright (c) 2014 Hanover. All rights reserved.
//

import Foundation
import UIKit

struct Hourly {
    var temperature: [String]!
    var icon: [UIImage]!
    var time: [String]!
	var rain: [Double]!
    
    init(weatherDict: NSDictionary) {
        temperature = [String]()
        icon = [UIImage]()
        time = [String]()
		rain = [Double]()
        
        let hourlyWeather = weatherDict["hourly"] as NSDictionary
        let hourlyWeatherTotal = hourlyWeather["data"] as NSArray
        
        for i in 0...23 {
            let thisWeather = hourlyWeatherTotal[i] as NSDictionary
            
            let tempInt = thisWeather["temperature"] as Int
            self.temperature.append("\(tempInt)º")
            
            let unixTime = thisWeather["time"] as Int
            
            self.time.append(constantFunc().timeStringFromUnixTime(unixTime))
            
            if i == 0 {
                self.time[0] = "Now"
            }
			
			let rainProb = thisWeather["precipProbability"] as Double
			self.rain.append(rainProb*100)
            
            let iconString = thisWeather["icon"] as String
            self.icon.append(constantFunc().weatherIconFromString(iconString))
        }
    }
        
}