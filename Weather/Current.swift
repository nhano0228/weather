//
//  Current.swift
//  Weather
//
//  Created by Noah Hanover on 9/22/14.
//  Copyright (c) 2014 Hanover. All rights reserved.
//

import Foundation
import UIKit

struct Current {
    var time: Int
    var temperature: Int
    var humidity: Double
    var summary: String
    var icon: UIImage?
    var wind: Double
    var date: String
    
    init(weatherDictionary: NSDictionary) {
        
        let currentWeather = weatherDictionary["currently"] as NSDictionary
        
        temperature = currentWeather["temperature"] as Int
        humidity = currentWeather["humidity"] as Double
        summary = currentWeather["summary"] as String
        wind = currentWeather["windSpeed"] as Double
        
        let currentTime = currentWeather["time"] as Int
        time = constantFunc().roundingTimeFromUnix(currentTime)
        date = constantFunc().dateStringFromUnixTime(currentTime)
        
        let iconString = currentWeather["icon"] as String
        icon = constantFunc().weatherIconFromString(iconString)
    }
    
}