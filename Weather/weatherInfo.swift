//
//  weatherInfo.swift
//  Weather
//
//  Created by Noah Hanover on 9/25/14.
//  Copyright (c) 2014 Hanover. All rights reserved.
//

import Foundation
import UIKit

class weatherInfoCell: UICollectionViewCell {
    
    var temperatureBottom: UILabel!
    var topLabel: UILabel!
	var middleRain: UILabel!
    var middleImage: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       
        let textFrame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height/5)
        topLabel = UILabel(frame: textFrame)
        topLabel.font = UIFont(name: "Roboto-Light", size: 15)
        topLabel.textColor = UIColor.whiteColor()
        topLabel.textAlignment = .Center
        contentView.addSubview(topLabel)
		
        middleImage = UIImageView(frame: CGRect(x: 0, y: textFrame.height-14, width: frame.size.width, height: frame.size.height))
        middleImage.contentMode = UIViewContentMode.ScaleAspectFit
        contentView.addSubview(middleImage)
		
		middleRain = UILabel(frame: CGRect(x: self.frame.width/4-1, y: 21.5, width: 25, height: 10))
		middleRain.textColor = UIColor.whiteColor()
		middleRain.textAlignment = .Center
		middleRain.font = UIFont(name: "Roboto-Regular", size: 14)
		contentView.addSubview(middleRain)
		
        let temperatureFrame = CGRect(x: 2, y: 47, width: frame.size.width, height: frame.size.height/4)
        temperatureBottom = UILabel(frame: temperatureFrame)
        temperatureBottom.textColor = UIColor.whiteColor()
        temperatureBottom.font = UIFont(name: "Roboto-Light", size: 15)
        temperatureBottom.textAlignment = .Center
        contentView.addSubview(temperatureBottom)
    }

    override func prepareForReuse() {
        temperatureBottom.text = ""
        topLabel.text = ""
        middleImage.image = UIImage()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}