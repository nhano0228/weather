//
//  constantFunc.swift
//  Weather
//
//  Created by Noah Hanover on 9/23/14.
//  Copyright (c) 2014 Hanover. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

struct constantFunc {
    
    func timeStringFromUnixTime (unixTime: Int) -> String {
        let timeInSeconds = NSTimeInterval(unixTime)
        let weatherDate = NSDate(timeIntervalSince1970: timeInSeconds)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .ShortStyle
        dateFormatter.AMSymbol = ""
        dateFormatter.PMSymbol = ""
        return dateFormatter.stringFromDate(weatherDate)
    }
    
    func roundingTimeFromUnix (unixTime: Int) -> Int {
        let timeInSeconds = NSTimeInterval(unixTime)
        let weatherDate = NSDate(timeIntervalSince1970: timeInSeconds)
        
        var currentCalender = NSCalendar.currentCalendar()
        
        var comps = currentCalender.component(NSCalendarUnit.HourCalendarUnit, fromDate: weatherDate)
        
        return comps
    }
    
    func dateStringFromUnixTime(unixTime: Int) -> String {
        let timeInSeconds = NSTimeInterval(unixTime)
        let weatherDate = NSDate(timeIntervalSince1970: timeInSeconds)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayOfWeekString = dateFormatter.stringFromDate(weatherDate)
        return dateFormatter.stringFromDate(weatherDate)
    }
    
    func backgroundIMG (currentTime: Int) -> UIImage {
        var background: UIImage
        switch currentTime {
        case 1...4:
            background = UIImage(named: "Background-Night")!
        case 5...11:
            background = UIImage(named: "Background")!
        case 12...18:
            background = UIImage(named: "Background-Afternoon")!
        case 19...24:
            background = UIImage(named: "Background-Night")!
        default:
            background = UIImage(named: "Background")!
        }
        return background
    }
    
    func weatherIconFromString (weatherString: String) -> UIImage {
        var imageName = ""
        
        switch weatherString {
        case "clear-day":
            imageName = "clear"
        case "clear-night":
            imageName = "nt_clear"
        case "rain":
            imageName = "rain"
        case "snow":
            imageName = "snow"
        case "sleet":
            imageName = "sleet"
        case "fog":
            imageName = "fog"
        case "cloudy":
            imageName = "cloudy"
        case "partly-cloudy-day":
            imageName = "partlycloudy"
        case "partly-cloudy-night":
            imageName = "nt_partlycloudy"
        case "wind":
            imageName = "wind"
        default:
            imageName = "default"
        }
        return UIImage(named: imageName)!
    }
    
}
