//
//  HorizontalWeather.swift
//  Weather
//
//  Created by Noah Hanover on 9/25/14.
//  Copyright (c) 2014 Hanover. All rights reserved.
//

import Foundation
import UIKit

class HorizontalWeather: UICollectionView {
    
    var cellTopText: String!
    var cellMiddleIcon: UIImage!
    var cellBottomText: String!
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.backgroundColor = UIColor(white: 35/100, alpha: 65/100)
        var flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        flowLayout.itemSize = CGSizeMake(45, 60)
        self.collectionViewLayout = flowLayout
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}